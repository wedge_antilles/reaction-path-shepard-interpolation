# Reaction Path Shepard Interpolation

Provides a python script for visualizing a reaction path on multiple energy levels. The interpolation method is adjustable by a parameter P, which enables to choose visualizations between broad plateaus and narrow ascents/descents.

Example for the following sampling points:

points = np.array([6, 7, 3, 4, 5, 2, 3, 4, -5, 0])

![Interpolated sample points](./shepard.png)
