#!/usr/bin/python
# Copyright (C) 2020 https://gitlab.com/wedge_antilles
import numpy as np
import matplotlib.pyplot as plt

# Computes weighting function at point x for sampling point a and P = p_param
def w(x,a,p_param):
    y=(1/pow(pow((x-a),2),(p_param/2)))
    return y

def prepare_data(p_param,points,file_name):
    # Generate equidistant x-coordinates for values y (points)
    x_sample=np.arange(0,len(points))

    # Sets step size of interpolant domain
    h_step=(x_sample[-1]+0.1)/10000.0

    # Sets interpolant domain
    x_inter=np.arange(x_sample[0]-0.1, x_sample[-1]+0.1,h_step)

    # Weight matrix holding [#interpolantPoints x #samplingPoints] entries
    w_matrix=np.zeros((np.size(x_inter),np.size(points)))

    # Filling with weights
    i_set=np.arange(0, np.size(points), 1)
    for i_count in i_set:
        j_set=np.arange(0, np.size(x_inter), 1)
        for j_count in j_set:
            w_matrix[j_count,i_count]=w(x_inter[j_count],
                                        x_sample[i_count],p_param)

    # Initiating interpolant values to be computed
    y_inter=np.zeros(np.size(x_inter))
    i_set=np.arange(0, np.size(y_inter), 1)

    # Interpolant enumerator: apply w_matrix onto sampling point vector
    # Interpolant denominator: sum up w_matrix entries on the fly
    for i_count in i_set:
        sum=0
        sum2=0
        j_set=np.arange(0, np.size(points), 1)
        for j_count in j_set:
            sum=sum+w_matrix[i_count,j_count]*points[j_count]
            sum2=sum2+w_matrix[i_count,j_count]
        y_inter[i_count]=sum/sum2
    plot_it(x_sample,points,p_param,x_inter,y_inter,file_name)

# Sets some plotting parameters and plots
def plot_it(x_sample,points,p_param,x_inter,y_inter,file_name):
    linestyle=['-']
    plt.xlim([x_sample[0]-0.1,x_sample[-1]+0.1])
    plt.ylim([np.amin(points)-0.1*np.abs(np.amin(points)),
              np.amax(points)+0.1*np.abs(np.amax(points))])
    plt.plot(x_inter,y_inter,label='P = '+str(p_param),
            linestyle=linestyle[0],linewidth=3.0, zorder=1)

    frame1 = plt.gca()
    frame1.axes.get_xaxis().set_ticks([i_count for i_count in x_sample])
    plt.xlabel('Reaction State', fontsize=20)
    plt.ylabel(r'$\Delta E$ / $\frac{kCal}{mol}$', fontsize=20)
    plt.title('Shepard-interpolated Reaction Path', fontsize=20)
    plt.grid()
    plt.legend(handlelength=5)
    plt.savefig(file_name)
    plt.show()

def main():
    file_name = "./shepard.png"
    points = np.array([6,7,3,4,5,2,3,4,-5,0])
    p_param = 4
    prepare_data(p_param, points, file_name)

main()
